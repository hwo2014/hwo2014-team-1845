module TrackPieces

  class Straight

    attr_reader :index, :length
    attr_accessor :optimal_lane

    def initialize(index, data)
      @index = index
      @length = data[:length]
      @switch = data.include?(:switch)
    end

    def length_on(lane)
      self.length
    end

    def switch?
      @switch
    end

    def curve?
      false
    end

    def straight?
      true
    end

  end

end
