module TrackPieces

  class Curve

    attr_reader :index, :angle, :radius
    attr_accessor :optimal_lane

    def initialize(index, data)
      @index = index
      @radius = data[:radius]
      @angle = data[:angle]
      @switch = data.include?(:switch)
    end

    def curve?
      true
    end

    def straight?
      false
    end

    def switch?
      @switch
    end

    def length
      @length ||= calculate_length(self.class.center_lane)
    end

    def length_on(lane)
      calculate_length(lane)
    end

    def left?
      self.angle < 0
    end

    def right?
      self.angle > 0
    end

    def lane_radius(lane)
      if self.right?
        self.radius - lane.center_offset
      else
        self.radius + lane.center_offset
      end.to_f
    end

    protected
    def calculate_length(lane)
      2.to_f * self.lane_radius(lane) * Math::PI * (self.angle.abs.to_f / 360.to_f)
    end

    def self.center_lane
      @center_lane ||= ::Lane.new(:distanceFromCenter => 0, :index => 0)
    end


  end

end
