module Listeners

  class Turbo
    
    def call(game, type, data)
      puts 'Turbo available'
      game.turbo = ::Turbo.new(game, data)
    end

  end

end
