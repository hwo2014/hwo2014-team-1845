module Listeners

  class GameStart

    def call(game, type, data)
      game.throttle = 1.0
    end

  end

end
