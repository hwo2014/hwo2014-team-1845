module Listeners

  class MaxAngleMeasurement

    def call(game, type, data)
      if type == :carPositions
	game.throttle = 1.0 unless game.throttle == 1.0

      elsif type == :crash
        if game.car(data[:color]) == game.own_car
          last_track_piece = game.race.track_pieces[game.own_car.positions[-2].piece_index]
          if last_track_piece && last_track_piece.curve?
            last_position = game.own_car.positions[-2]
            game.centrifugal = (last_position.speed ** 2) * last_track_piece.lane_radius(last_position.lane)
            game.connection.unregister :carPositions, self
            game.connection.unregister :crash, self
          end
        end
      end
    end

  end

end
