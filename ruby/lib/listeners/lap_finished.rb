module Listeners

  class LapFinished

    def call(game, type, data)
      if game.own_car.color == data[:car][:color]

        lap = data[:lapTime][:lap]
        ticks = data[:lapTime][:ticks]
        millis = data[:lapTime][:millis]

        overall_rank = data[:ranking][:overall]
        fastest_lap = data[:ranking][:fastestLap]

        puts "Finished Lap ##{lap + 1} in #{(millis.to_f / 1000).round(2)}s"
      end
    end

  end

end
