module Listeners

  class StaticThrottle

    def call(game, type, data)
      amount = 1.0 

      if game.current_track_piece.switch?
        if game.own_car.end_lane_index != game.current_track_piece.optimal_lane.index
          if direction = game.race.switch_direction_for(
              game.race.lanes[game.own_car.end_lane_index],
              game.current_track_piece.optimal_lane
            )
            game.switch_lane(direction)
          end
        end
      end
  

      if game.turbo_available?
        min_length_straight = 300
        if game.current_track_piece.straight?
          distance = game.current_track_piece.length - game.own_car.in_piece_distance
          1.upto(5) do |index|
            if game.current_track_at(index).straight?
              distance += game.current_track_at(index).length
            else
              break
            end
          end
          if distance >= min_length_straight
            puts "using turbo"
            game.turbo.use!
          end
        end
      end

      if game.centrifugal && game.drag
        min_radius = game.race.track_pieces.collect do |piece|
          if piece.curve?
            piece.radius
          else
            999
          end
        end.min

        min_radius += game.race.lanes.collect(&:center_offset).min

        max_speed = Math.sqrt(game.centrifugal / min_radius)
        amount = max_speed * game.drag * 0.9 
      end

      game.throttle = amount 
      
    end

  end

end
