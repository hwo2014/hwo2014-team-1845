module Listeners
  module Pathfinder

    class Path

      attr_reader :lane, :track_piece

      def initialize(lane, track_piece)
        @lane = lane
        @track_piece = track_piece
      end

      def length
        self.track_piece.length_on(self.lane)
      end

    end

  end
end
