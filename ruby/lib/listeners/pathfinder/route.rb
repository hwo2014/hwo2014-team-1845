module Listeners
  module Pathfinder

    class Route

      attr_writer :paths

      def initialize(paths = [])
        @paths = paths
      end

      def paths
        @paths ||= []
      end

      def length
        self.paths.collect(&:length).inject(:+)
      end

      def switch_count
        @switches ||= begin
          switches = 0
          current_index = self.paths.first.lane.index
          self.paths.each do |path|
            if path.lane.index != current_index
              current_index = path.lane.index
              switches += 1
            end
          end
          switches
        end
      end

      def clone
        @paths = self.paths.clone
        super
      end

    end

  end
end
