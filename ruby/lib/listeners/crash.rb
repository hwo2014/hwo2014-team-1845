module Listeners

  class Crash
  
    def call(game, type, data)
      if car = game.car(data[:color])
        puts "#{car.name} crashed!"
        car.crashed = true
        if game.own_car == car
          game.turbo = nil
        end
      end
    end

  end

end
