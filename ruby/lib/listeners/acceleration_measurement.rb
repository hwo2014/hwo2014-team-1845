module Listeners
  
  class AccelerationMeasurement

    def initialize
      @speed = []
      @distance_before = nil
    end

    def call(game, type, data)
      @speed ||= []
      @distance_before ||= game.own_car.in_piece_distance || 0
      if @speed.length < 3
        if game.own_car.in_piece_distance > 0
    	  distance = game.own_car.in_piece_distance
          covered = distance - @distance_before
          @distance_before = distance
          @speed << covered 
        end
        game.throttle = 1.0
      else
        game.drag = drag = (@speed[0] - (@speed[1] - @speed[0])) / @speed[0]**2 * 1.0

        game.mass = 1.0 / (Math.log((@speed[2] - (1.0 / drag)) / (@speed[1] - (1.0 / drag))) / (drag * -1))

        game.connection.unregister :carPositions, self
      end
    end

  end

end
