module Listeners

  class Spawn
  
    def call(game, type, data)
      if car = game.car(data[:color])
	puts "#{car.name} spawned!"
        car.crashed = false 
        if car == game.own_car
          game.turbo = nil
        end
      end
    end

  end

end
