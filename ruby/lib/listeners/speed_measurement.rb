module Listeners

  class SpeedMeasurement

    def call(game, type, data)

      game.all_cars.each do |car|
        if car.positions.size > 1
          second_last = car.positions[-2]
          last = car.positions.last
          distance = distance_between(second_last, last)
          car.speed = distance
        end
      end

    end

    def distance_between(position1, position2)
      if position1.piece_index == position2.piece_index
        return position2.in_piece_distance - position1.in_piece_distance
      else
        distance_from_track_end = if position1.in_piece_distance > position1.full_length
          0
        else
          position1.full_length - position1.in_piece_distance
        end
        distance_from_track_end + position2.in_piece_distance
      end
    end

  end

end
