module Listeners

  class OwnCarRegistrar

    def call(game, type, data)
      color = data[:color]
      puts "Your car is #{color}"
      game.own_car_color = color
    end
    
  end

end
