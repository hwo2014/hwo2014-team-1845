module Listeners

  class GameInit

    def call(game, type, data)
      puts 'GameInit'
      game.race = race = Race.new

      data[:race][:track][:pieces].each_with_index do |data, index|
        race.track_pieces[index] = if data.include?(:length)
          TrackPieces::Straight.new(index, data)
        else
          TrackPieces::Curve.new(index, data)
        end
      end
      race.length = race.track_pieces.collect(&:length).inject(:+)

      data[:race][:track][:lanes].each do |data|
        game.race.lanes << Lane.new(data)
      end

      data[:race][:cars].each do |data|
        car_color = data[:id][:color]
        game.add_car(car_color, Car.new(data))
      end

     raceSession = data[:race][:raceSession]

     if raceSession.include?(:durationMs)
       puts 'starting qualification'
       race.type = :qualification
       race.duration = raceSession[:durationMs]
     else
       puts 'starting race'
       race.type = :race
       race.laps = raceSession[:laps]
       race.max_lap_time = raceSession[:maxLapTimeMs]
       race.quick_race = raceSession[:quickRace]
     end

      # pathfinding
      routes = game.race.lanes.collect do |lane|
        find_routes(game, [], lane, 0)
      end.flatten


      routes_by_length = routes.group_by(&:length)
      min_key = routes_by_length.keys.min
      shortest_route = if routes_by_length[min_key].length > 1

        routes_by_length[min_key].min_by(&:switch_count)
      else
        routes_by_length[min_key].first
      end
      
      shortest_route.paths.each_with_index do |path, index|
        path.track_piece.optimal_lane = path.lane
      end
      

      # TODO check if its better to switch lane before or after lap, if first and last lane dont match

    end

    def find_routes(game, paths, start_lane, track_piece_index)
      return Pathfinder::Route.new(paths.clone) unless game.race.track_pieces[track_piece_index]
      
      track_piece = game.race.track_pieces[track_piece_index]

      lanes = [start_lane]
      lanes += game.race.possible_lanes(start_lane) if track_piece.switch?

      return lanes.collect do |lane|
        paths << Pathfinder::Path.new(lane, track_piece)
        routes = find_routes(game, paths, lane, track_piece_index + 1)
        paths.pop
        routes
      end
    end

  end

end
