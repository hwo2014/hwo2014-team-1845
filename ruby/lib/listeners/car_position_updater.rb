module Listeners

  class CarPositionUpdater

    def call(game, type, data)
      data.each do |car_data|
        car_color = car_data[:id][:color]
        if car = game.car(car_color)
          car.angle = car_data[:angle]
          car.piece_index = car_data[:piecePosition][:pieceIndex]
          car.in_piece_distance = car_data[:piecePosition][:inPieceDistance]
          car.start_lane_index = car_data[:piecePosition][:lane][:startLaneIndex]
          car.end_lane_index = car_data[:piecePosition][:lane][:endLaneIndex]
          car.lap = car_data[:piecePosition][:lap]
        end
      end
      # game.tick = data[:gameTick]
    end

  end

end
