class Game

  attr_reader :throttle, :connection, :tick
  attr_reader :active_turbo
  attr_accessor :race, :own_car_color, :drag, :mass, :centrifugal 

  def initialize(connection, sender)
    @connection = connection
    @sender = sender
    @tick = 0
  end

  def current_track_piece(car = self.own_car)
    self.race.track_pieces[car.piece_index]
  end

  def current_track_at(position, car = self.own_car)
    requested_position = car.piece_index + position
    max_index = self.race.track_pieces.length - 1
    if requested_position > max_index
      requested_position = max_index - requested_position
    elsif requested_position < 0
      requested_position = max_index - requested_position
    end

    self.race.track_pieces[requested_position]
  end

  def add_car(color, car)
    self.cars[color] = CarTick.new(self, car)
  end

  def car(color = self.own_car_color)
    self.cars[color]
  end

  def cars
    @cars ||= {}
  end

  def own_car
    self.car
  end

  def all_cars
    self.cars.values 
  end

  def turbo=(turbo)
    @available_turbo = turbo
  end

  def turbo
    @available_turbo
  end

  def turbo_available?
    @available_turbo
  end

  def turbo_active?
    @active_turbo
  end

  def tick=(tick)
    @active_turbo = nil if tick > @turbo_finished_at.to_i
    @sender.release_lock
    @tick = tick
  end

  def lock
    @sender.lock
  end

  # messages
  def join(bot_name, bot_key)
    @sender.join(bot_name, bot_key)
  end
  
  def create(bot_name, bot_key, track_name)
    @sender.create(bot_name, bot_key, track_name)
  end

  def switch_lane(direction)
    @sender.switch_lane(direction)
  end

  def throttle=(amount)
    if @sender.throttle(amount)
      @throttle = amount
    end
  end

  def use_turbo!(message = nil)
    @active_turbo = @available_turbo
    @turbo_finished_at = self.tick + @active_turbo.duration_ticks
    @available_turbo = nil
    @sender.turbo(message)
  end

  def turbo?
    self.turbo
  end

end
