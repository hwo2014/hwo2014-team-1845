class Race

  attr_accessor :length, :type, :duration, :laps, :max_lap_time, :quick_race

  def track_pieces
    @track_pieces ||= []
  end

  def lanes
    @lanes ||= []
  end

  def switch_direction_for(from, to)
    if from.center_offset > to.center_offset
      'Left'
    elsif from.center_offset < to.center_offset
      'Right'
    else
      nil
    end
  end

  def possible_lanes(lane)
    index = self.lanes.index(lane)
    if index == 0
      return [self.lanes[1]] if self.lanes[1]
      return []  
    elsif self.lanes[index + 1]
      return [self.lanes[index - 1], self.lanes[index + 1]]
    else
      return [self.lanes[index - 1]]
    end
  end

  def print
    self.track_pieces.each do |piece|
      puts piece.print
    end
  end

end
