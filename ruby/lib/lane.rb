class Lane

  attr_reader :center_offset, :index

  # positiv right
  # negativ left
  def initialize(data)
    @center_offset = data[:distanceFromCenter]
    @index = data[:index]
  end

  def direction_from_center
    if self.center_offset > 0
      'Right'
    elsif self.center_offset < 0
      'Left'
    else
      nil
    end
  end



end
