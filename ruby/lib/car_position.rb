class CarPosition

  attr_accessor :angle, :piece_index, :in_piece_distance, :lap
  attr_accessor :start_lane_index, :end_lane_index
  attr_accessor :speed, :crashed

  def initialize(game)
    @game = game
    @speed = 0
    @crashed = false
  end
  
  def length
    return self.in_piece_distance
  end

  def full_length
    lane = @game.race.lanes[self.start_lane_index]
    track_piece = @game.race.track_pieces[self.piece_index]
    return track_piece.length_on(lane)
  end

  def switching?
    self.start_lane_index != self.end_lane_index
  end

  def crashed?
    @crashed
  end

  def lane
    @lane ||= begin
      if self.start_lane_index == self.end_lane_index
        @game.race.lanes[self.start_lane_index]
      elsif self.in_piece_distance < (@game.race.track_pieces[self.piece_index].length / 2)
        @game.race.lanes[self.start_lane_index]
      else
        @game.race.lanes[self.end_lane_index]
      end
    end
  end

end
