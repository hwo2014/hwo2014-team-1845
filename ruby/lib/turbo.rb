class Turbo

  attr_reader :duration_milis, :duration_ticks, :factor

  def initialize(game, data)
    @game = game
    @duration_milis = data[:turboDurationMilliseconds]
    @duration_ticks = data[:turboDurationTicks]
    @factor = data[:turboFactor]
  end

  def use!(message = nil)
    @game.use_turbo!(message)
  end

end
