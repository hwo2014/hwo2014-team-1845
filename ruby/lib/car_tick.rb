require 'forwardable'

class CarTick

  extend Forwardable

  def_delegators :@car, :name, :color, :length, :width, :guide_flag_position, :positions
  def_delegators :position, :angle, :piece_index, :in_piece_distance, :lap, :start_lane_index, :end_lane_index
  def_delegators :position, :angle=, :piece_index=, :in_piece_distance=, :lap=, :start_lane_index=, :end_lane_index=
  def_delegators :position, :speed, :speed=, :crashed, :crashed=, :crashed?

  def initialize(game, car)
    @game = game
    @car = car
  end

  def position
    @car.positions[@game.tick] ||= CarPosition.new(@game)
  end

end
