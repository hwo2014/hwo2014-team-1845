class MessageSender

  def initialize(socket)
    @socket = socket
    @lock = false
  end

  def join(bot_name, bot_key)
    send_typed_message(:join, {:name => bot_name, :key => bot_key})
  end

  def create(bot_name, bot_key, track_name)
    send_typed_message(:createRace, {
      :botId => {:name => bot_name, :key => bot_key},
      :trackName => track_name,
      :password => "s3ccreattt",
      :carCount => 1
    })
  end

  def throttle(amount)
    amount = 1.0 if amount > 1.0
    send_typed_message(:throttle, amount)
  end

  def switch_lane(direction)
    send_typed_message(:switchLane, direction)
  end

  def turbo(message = "pew pew superspeed")
    send_typed_message(:turbo, message)
  end

  def release_lock
    @lock = false
  end

  def lock
    @lock = true
  end
  protected
  def send_typed_message(type, data)
    send_message({:msgType => type, :data => data})
  end

  def send_message(message)
    unless @lock
      json = JSON.generate(message)
      logger.info json
      @socket.puts json
      return @lock = true
    else
      return false
    end
  end

  def logger
    @logger ||= ::Logger.new(SOCKET_LOG)
  end

end
