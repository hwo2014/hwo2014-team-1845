class Car

  attr_reader :name, :color, :length, :width, :guide_flag_position

  def initialize(data)
    @name = data[:id][:name]
    @color = data[:id][:color]
    @length = data[:dimensions][:length]
    @width = data[:dimensions][:width]
    @guide_flag_position = data[:dimensions][:guideFlagPosition]
  end

  def positions
    @positions ||= []
  end

end
