class Connection

  attr_reader :game

  def initialize(server_host, server_port)
    @server_host = server_host
    @server_port = server_port
    @socket = nil
    @listeners = {}
  end

  def register(type, listener)
    @listeners[type] ||= []
    @listeners[type] << listener
  end

  def unregister(type, listener)
    @listeners[type].delete(listener)
  end

  def listen!
    while json = self.socket.gets
      socket_logger.info json
      message = JSON.parse(json, :symbolize_names => true)
      type = message[:msgType].to_sym
      data = message[:data]
      @game.tick = message[:gameTick] if message.include?(:gameTick)
      start = Time.now
      if listeners = @listeners[type]
        listeners.each do |listener|
          listener.call(@game, type, data)
        end
      else
        puts "Unhandled messageType #{type}"
      end
      elapsed = Time.now - start
      self.iteration_logger.info "#{(elapsed * 1000).to_i}ms / #{(1 / elapsed).to_i}/sec"
    end
  end

  def game
    @game ||= Game.new(
      self,
      MessageSender.new(self.socket)
    )
  end

  protected
  def socket
    @socket ||= begin
      TCPSocket.open(
        @server_host,
        @server_port
      )
    end
  end

  def socket_logger
    @socket_logger ||= ::Logger.new(SOCKET_LOG)
  end

  def iteration_logger
    @iteration_logger ||= ::Logger.new(ITERATION_LOG)
  end 

end
