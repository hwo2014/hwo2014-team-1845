require 'json'
require 'logger'
require 'socket'
 
Dir[File.join(Dir.pwd, 'lib', '**', '*.rb')].each do |file|
  require file
end

SOCKET_LOG = File.join(Dir.pwd, 'logs', 'socket.log')
ITERATION_LOG = File.join(Dir.pwd, 'logs', 'iteration.log')

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]
track_name = ARGV[4]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class BackgroundBot

  def initialize(server_host, server_port)
    @connection = Connection.new(server_host, server_port)

    trash = Proc.new{|game, data|}
    @connection.register :error, Listeners::Error.new
    @connection.register :join, trash

    @connection.register :yourCar, Listeners::OwnCarRegistrar.new
    @connection.register :gameInit, Listeners::GameInit.new
    @connection.register :gameStart, Listeners::GameStart.new
    @connection.register :lapFinished, Listeners::LapFinished.new

    max_angle_measurement_listener = Listeners::MaxAngleMeasurement.new

    @connection.register :crash, Listeners::Crash.new
    @connection.register :crash, max_angle_measurement_listener
    @connection.register :spawn, Listeners::Spawn.new

    @connection.register :carPositions, Listeners::CarPositionUpdater.new
    @connection.register :carPositions, Listeners::AccelerationMeasurement.new
    @connection.register :carPositions, max_angle_measurement_listener
    @connection.register :carPositions, Listeners::SpeedMeasurement.new
    @connection.register :carPositions, Listeners::StaticThrottle.new
    @connection.register :turboAvailable, Listeners::Turbo.new
  end

  def join(bot_name, bot_key)
    @connection.game.join(bot_name, bot_key)
  end

  def create(bot_name, bot_key, track_name)
    @connection.game.create(bot_name, bot_key, track_name)
  end

  def start!
    @connection.listen!
  end

end

bot = BackgroundBot.new(server_host, server_port)
if track_name
  bot.create(bot_name, bot_key, track_name)
else
  bot.join(bot_name, bot_key)
end
bot.start!
